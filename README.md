# Dojo de REST
Esta é uma proposta de um Dojo utilizando conceitos de REST, utilizando Django, Django REST Framework e py.test.

Este dojo tem como objetivo construir um sistema de auxilido de marcação de dojos, e pode ser dividido em três partes:
1. Persistencia de uma proposta de dojo.
2. Verificação e autorização por pelo menos dois coordenadores
3. Envio automatico de email pedindo agendamento do LINF na data marcada.

## Estrutura da Respota
Uma chamada GET no endpoint _/dojos/_, poderia ter a seguinte resposta:
```
{
	[
		{
			title: "Dojo de Angular4",
			master: "Fulano de Tal",
			id: "/dojos/1"
		},
		{
			title: "TDD na Pratica com Haskell",
			master: "Cicrano de Tal",
			id: "/dojos/2"
		},
		{
			title: "A Familia jUnit",
			master: "Beltrano",
			id: "/dojos/3"
		},
	]
}
```
Já uma chamada GET em um endpoint utilizando um id especifico, como em _/dojos/1_, retorna um objeto em detalhes:
```
{
	self: "/dojos/1"
	title: "Dojo de Angular4",
	master: "Fulano de Tal",
	master_id: "/masters/1",
	sponsors: [
		{
			name: "Martin Fowler",
			id: "/masters/1"
		},
		{
			name: "Uncle Bob",
			id: "/masters/2"
		}
	],
	date: "20180705T14:00Z+0300",
	room_reserved: true,
}
```

A idéia é que qualquer pessoa possa submeter uma proposta para mestrar um Dojo. Assim que houverem dois _padrinhos_ autorizando a idéia, um email seria enviado automaticamente para a área de reservas do LINF/UnB.
